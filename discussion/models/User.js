
const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String
	},
	lastName: { 
		type:String 
	}
	email: { 
		type: String
	}
	password: { 
		type: String
	}
	isAdmin: {
		type: Boolean,
		default: false
	}
	mobileNo: {
		type: String
	}
	enrollments: [
		{
			courseId: {
				type: String
			}
			enrolledOn: {
				type: Date,
				default: new Date()
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema);